# Server-side Utilities
Server-side Utilities (SSUtils) is a server-side only mod for Minecraft 1.7.10 that adds a few administration tools.
SSUtils is minimal and only provides the functions that I personally require to run my servers.

## Usage
This mod has no dependencies. All available options are described in the mod's config file. If you can't figure out how to install and configure it, I advise you find someone capable of reading to set up your server for you.