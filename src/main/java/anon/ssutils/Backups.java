package anon.ssutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;

import org.apache.commons.io.FilenameUtils;

public class Backups {

    public static File backupsDirectory;

    public static void ensureDirectoryExists() {
        if (backupsDirectory == null) backupsDirectory = new File(Config.backupPath);
        if (!backupsDirectory.exists()) backupsDirectory.mkdirs();
    }

    // lol functional lmao
    public static void deleteOldBackups() {
        if (Config.backupRetainment == 0) return;

        ensureDirectoryExists();
        try {
            List<Pair<Path, LocalDateTime>> backups = Files.walk(backupsDirectory.toPath())
                .filter(Files::isRegularFile)
                .map(
                    path -> new Pair<>(
                        path,
                        tryExpressionOrNull(
                            () -> LocalDateTime.from(
                                DateTimeFormatter.ISO_LOCAL_DATE_TIME
                                    .parse(FilenameUtils.getBaseName(path.toString()))))))
                .filter(pair -> pair.second != null)
                .collect(Collectors.toList());

            long toDelete = Math.max(backups.size() - Config.backupRetainment, 0);
            if (toDelete == 0) return;

            backups.stream()
                .sorted((a, b) -> a.second.compareTo(b.second))
                .limit(toDelete)
                .forEach(
                    pair -> pair.first.toFile()
                        .delete());
        } catch (Exception e) {
            SSUtils.sendToChat(
                "An error has occured on the server, please report this to an administrator with a timestamp.");
            SSUtils.LOG.error("Failed to delete old backups: ", e);
        }
    }

    public static <R> R tryExpressionOrNull(Supplier<R> expression) {
        try {
            return expression.get();
        } catch (Exception e) {
            return null;
        }
    }

    public static void flushWorlds() {
        MinecraftServer server = MinecraftServer.getServer();

        for (WorldServer worldServer : server.worldServers) {
            boolean old = worldServer.levelSaving;

            try {
                worldServer.levelSaving = true;
                worldServer.saveAllChunks(true, null);
                worldServer.levelSaving = old;
            } catch (Exception e) {
                SSUtils.LOG.warn("Failed to flush world server chunks.", e);
            }
        }
    }

    public static void makeBackup(File source, File destination, int compression) {
        ensureDirectoryExists();

        long startTime = System.currentTimeMillis();

        SSUtils.sendToChat("Starting backup...");
        try {
            ZipOutputStream zipOutput = new ZipOutputStream(new FileOutputStream(destination));
            zipOutput.setLevel(compression);

            long nextLogTime = startTime;
            long totalFiles = Files.walk(source.toPath())
                .filter(Files::isRegularFile)
                .count(), processedFiles = 0;
            for (Path file : (Iterable<Path>) Files.walk(source.toPath())
                .filter(Files::isRegularFile)::iterator) {
                ZipEntry entry = new ZipEntry(
                    source.toPath()
                        .relativize(file)
                        .toString());
                FileInputStream fileInput = new FileInputStream(file.toFile());
                byte[] buffer = new byte[4096];
                int read;

                zipOutput.putNextEntry(entry);
                while ((read = fileInput.read(buffer)) > 0) zipOutput.write(buffer, 0, read);
                zipOutput.closeEntry();
                fileInput.close();
                processedFiles++;

                long currentTime = System.currentTimeMillis();
                if (currentTime > nextLogTime || processedFiles == totalFiles) {
                    nextLogTime = currentTime + Config.backupProgressAnnounceInterval;
                    SSUtils.sendToChat(
                        "Backup: [{0} | {1}%]",
                        processedFiles,
                        ((float) processedFiles / (float) totalFiles) * 100);
                }
            }

            zipOutput.close();

            float seconds = (System.currentTimeMillis() - startTime) / 1000.f;
            SSUtils.sendToChat(
                "Finished backup in {0} second{1} [{2} | {3}].",
                seconds,
                seconds == 1.f ? "" : "s",
                formatSizeSuffix(destination.length()),
                formatSizeSuffix(
                    Files.walk(backupsDirectory.toPath())
                        .filter(Files::isRegularFile)
                        .map(Path::toFile)
                        .mapToLong(File::length)
                        .sum()));
        } catch (Exception e) {
            SSUtils.sendToChat("Failed to create backup! Please report this to an administrator.");
            SSUtils.LOG.error("Failed to create backup:", e);
            destination.delete();
        }
    }

    public static String formatSizeSuffix(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %ciB", value / 1024.0, ci.current());
    }

    public static File getCurrentBackupFile() {
        ensureDirectoryExists();
        return new File(backupsDirectory, DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()) + ".zip");
    }
}
