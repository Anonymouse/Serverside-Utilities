package anon.ssutils;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class Config {

    public static String[] suicideLocalTimes;
    public static String messageOnJoin;

    public static String backupPath;
    public static float backupInterval;
    public static boolean backupEmpty;
    public static int backupRetainment;
    public static int backupCompression;
    public static int backupProgressAnnounceInterval;

    public static void synchronizeConfiguration(File configFile) {
        Configuration configuration = new Configuration(configFile);

        suicideLocalTimes = configuration.getStringList(
            "suicideLocalTimes",
            "general",
            new String[] { "04:00:00", "16:00:00" },
            "The times of day at which the server should shut itself down, leave empty to disable auto shutoff.");

        messageOnJoin = configuration.getString(
            "messageOnJoin",
            "general",
            "Welcome to the server!",
            "A chat message to be sent to players upon joining. Leave empty to send no message.");

        backupPath = configuration.getString("backupPath", "backup", "backups", "The path to the backups directory.");
        backupInterval = configuration.getFloat(
            "backupInterval",
            "backup",
            1.f,
            0.f,
            Float.POSITIVE_INFINITY,
            "How many hours to wait between backups. Set to 0 to disable backups.");
        backupEmpty = configuration.getBoolean(
            "backupEmpty",
            "backup",
            false,
            "Whether or not to create a backup when no players are online.");
        backupRetainment = configuration.getInt(
            "backupRetainment",
            "backup",
            4,
            0,
            Integer.MAX_VALUE,
            "How many automatic backups should be stored at once, once this number is exceeded, the backups with the earliest timestamps will be deleted. Files that do not have timestamps as names will not be considered.");
        backupCompression = configuration
            .getInt("backupCompression", "backup", 5, 0, 9, "The compression level at which backups are stored.");
        backupProgressAnnounceInterval = configuration.getInt(
            "backupProgressAnnounceInterval",
            "backup",
            5000,
            1000,
            Integer.MAX_VALUE,
            "The minimum number of milliseconds to wait before announcing the current backup's progress in chat.");

        if (configuration.hasChanged()) {
            configuration.save();
        }
    }
}
