package anon.ssutils;

import java.text.MessageFormat;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

@Mod(
    modid = Tags.MODID,
    version = Tags.VERSION,
    name = Tags.MODNAME,
    acceptedMinecraftVersions = "[1.7.10]",
    acceptableRemoteVersions = "*")
public class SSUtils {

    public static final Logger LOG = LogManager.getLogger(Tags.MODID);

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        Config.synchronizeConfiguration(event.getSuggestedConfigurationFile());
        FMLCommonHandler.instance()
            .bus()
            .register(this);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {}

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {}

    @Mod.EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        if (Config.suicideLocalTimes.length != 0) scheduleSuicide();
        if (Config.backupInterval != 0.f) {
            long delay = (long) (Config.backupInterval * 3600);
            scheduler.scheduleAtFixedRate(new Runnable() {

                public void run() {
                    MinecraftServer server = MinecraftServer.getServer();
                    if (server.getConfigurationManager().playerEntityList.isEmpty() && !Config.backupEmpty) return;

                    Backups.flushWorlds();
                    Backups.makeBackup(
                        server.getEntityWorld()
                            .getSaveHandler()
                            .getWorldDirectory(),
                        Backups.getCurrentBackupFile(),
                        Config.backupCompression);
                    Backups.deleteOldBackups();
                }
            }, delay, delay, TimeUnit.SECONDS);
        }
    }

    @SubscribeEvent
    public void playerLoggedIn(PlayerLoggedInEvent event) {
        if (Config.messageOnJoin.length() == 0) return;
        event.player.addChatMessage(new ChatComponentText(Config.messageOnJoin));
    }

    public static void sendToChat(String format, Object... args) {
        MinecraftServer.getServer()
            .getConfigurationManager()
            .sendChatMsg(new ChatComponentText("[ssutils] " + MessageFormat.format(format, args)));
    }

    private static void scheduleSuicide() {
        long minimumDelay = Long.MAX_VALUE;
        LocalTime currentTime = LocalTime.now();
        for (String timeString : Config.suicideLocalTimes) {
            long delay;
            try {
                LocalTime targetTime = LocalTime.parse(timeString);
                delay = currentTime.until(targetTime, ChronoUnit.SECONDS);
            } catch (Exception e) {
                LOG.error("Failed to parse entry in suicideLocalTimes '" + timeString + "'", e);
                continue;
            }

            if (delay < 0) delay += 86400;

            minimumDelay = Math.min(minimumDelay, delay);
        }

        scheduleAnnouncement(minimumDelay, 60, TimeUnit.MINUTES);
        scheduleAnnouncement(minimumDelay, 30, TimeUnit.MINUTES);
        scheduleAnnouncement(minimumDelay, 10, TimeUnit.MINUTES);
        scheduleAnnouncement(minimumDelay, 5, TimeUnit.MINUTES);

        scheduleAnnouncement(minimumDelay, 60, TimeUnit.SECONDS);
        scheduleAnnouncement(minimumDelay, 30, TimeUnit.SECONDS);
        scheduleAnnouncement(minimumDelay, 10, TimeUnit.SECONDS);
        scheduleAnnouncement(minimumDelay, 5, TimeUnit.SECONDS);

        scheduler.schedule(new Runnable() {

            public void run() {
                MinecraftServer.getServer()
                    .initiateShutdown();
            }
        }, minimumDelay, TimeUnit.SECONDS);
    }

    private static void scheduleAnnouncement(long target, long delta, TimeUnit unit) {
        long adjusted = TimeUnit.SECONDS.convert(delta, unit);
        if ((target - adjusted) < 0) return;

        scheduler.schedule(new Runnable() {

            public void run() {
                sendToChat(
                    "Server will restart in {0} {1}.",
                    delta,
                    unit.name()
                        .toLowerCase());
            }
        }, target - adjusted, TimeUnit.SECONDS);
    }
}
